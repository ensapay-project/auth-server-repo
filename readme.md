# ENSAPAY Auth Server

Auth Server is the central authentication manager based on [Keycloak](https://www.keycloak.org). It's used to secure application resources by providing authentication and role based authorization out of the box.

## Configuration

You need to create a configuration file `application.yml` :

```bash
$ cp application.yml.example application.yml
```

And setup the variables according to your database in `application.yml` :

```yaml
spring:
  datasource:
    url: jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_NAME}
    username: ${DB_USER}
    password: ${DB_PASS}
```
Also in `/resouces/META-INF/keycloak-server.json` in `connectionsJpa`:
```json
"connectionsJpa": {
        "default": {
            "url": "${keycloak.connectionsJpa.url:jdbc:mysql://<host>:<port>/<db_name>}",
            "driver": "${keycloak.connectionsJpa.driver:com.mysql.jdbc.Driver}",
            "driverDialect": "${keycloak.connectionsJpa.driverDialect:}",
            "user": "${keycloak.connectionsJpa.user:<db_user>}",
            "password": "${keycloak.connectionsJpa.password:<db_password>}",
            "initializeEmpty": true,
            "migrationStrategy": "update",
            "showSql": "${keycloak.connectionsJpa.showSql:false}",
            "formatSql": "${keycloak.connectionsJpa.formatSql:true}",
            "globalStatsInterval": "${keycloak.connectionsJpa.globalStatsInterval:-1}"
        }
    }
```
Last thing, you need to setup crendentials for default admin account :

```yaml
keycloak:
  server:
    contextPath: /auth
    adminUser:
      username: ${KEYCLOAK_USERNAME}
      password: ${KEYCLOAK_PASS}
```

Then launch the application, and navigate to:

```sh
http://localhost:8083/auth
```

And enter admin credentials that you've set up earlier.

## Few usage examples

To create a user with a rest API, you need first to authenticate as admin and get the `access token` :

```sh
curl --location --request POST 'http://localhost:8083/auth/realms/master/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=admin2' \
--data-urlencode 'password=admin2' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_id=admin-cli'
```

If the request is successful, in response you should get an access token :

```json
{ "access_token": "xxxxxx" }
```

And send a POST request with the `bearer token` :

```sh
curl --location --request POST 'http://localhost:8083/auth/admin/realms/ensapay/users' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer xxxxxx' \
--data-raw '{"firstName":"Oussama","lastName":"Lahmidi", "email":"test@test.com", "enabled":"true", "username":"oussa", "credentials": [{ "type": "password", "temporary": false, "value": "<secret-password>" }]}'
```

You can authenticate with newly registered in the browser by navigating to this link :

```
http://localhost:8080/auth/realms/ensapay/account
```

or you can login statelessfully to get an `access token` :

```sh
curl --location --request POST 'http://localhost:8083/auth/realms/ensapay/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=oussa' \
--data-urlencode 'password=<secret-password>' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_id=resource-server'
```

## Author

LAHMIDI Oussama

