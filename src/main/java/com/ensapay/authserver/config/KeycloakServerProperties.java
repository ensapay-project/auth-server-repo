package com.ensapay.authserver.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "keycloak.server")
public class KeycloakServerProperties {
    String contextPath = "/auth";
    String realmImportFile = "baeldung-realm.json";
    AdminUser adminUser = new AdminUser();

    public String getContextPath() {
        return contextPath;
    }

    public String getRealmImportFile() {
        return realmImportFile;
    }

    public AdminUser getAdminUser() {
        return adminUser;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public void setRealmImportFile(String realmImportFile) {
        this.realmImportFile = realmImportFile;
    }

    public void setAdminUser(AdminUser adminUser) {
        this.adminUser = adminUser;
    }

    // getters and setters

    public static class AdminUser {
        String username = "admin";
        String password = "admin";

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        // getters and setters
    }
}
